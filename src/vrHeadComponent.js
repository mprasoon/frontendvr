import 'aframe';
import React from 'react';
import {Box, Icosahedron, Cylinder, Plane, Sky, Text, Scene} from 'react-aframe-ar'
import axios from 'axios';

class VrBodyComponent extends React.Component {
  state = {
    box1Pos: "-10 0.5 -3",
    box2Pos: "10 0.5 -3",
    box3Pos: "0 0.5 -24",
    planePos: "0 0 -4",
    box1PosXinit: -10,
    box2PosXinit: 10,
    box3PosXinit: 0,
    planePosXinit: 0,
    box: {
      pos: '-1 0.5 -3',
      rot: '0 0 0',
      color: '#4CC3D9'
    },
    sphere: {
      pos: '0 1.25 -5',
      rad: 1.25,
      color: '#EF2D5E'
    },
    cylinder: {
      pos: '2 0.75 -3',
      rad: 0.5,
      ht: 1.5,
      color: '#FFC65D'
    },
    plane: {
      pos: '0 0 -4',
      rot: '-90 0 0',
      width: 20,
      ht: 40,
      color: '#7BC8A4'
    },
    sphere_list: [],
    loading: true,
    tilt: 0,
    t: 0
  }
  get_user_pos = () => {
    const user_db_id = this.props.match.params.user_db_id
    const {tilt} = this.state
    const maxD = 8
    let get_url = 'http://localhost:8000/api/getdata/'+user_db_id+'/'
    this.setState(prevState => {
      const newState = prevState
      newState['t'] = prevState['t'] + 1
      let tiltRound = Number(Number(tilt).toFixed(1))
      let box1PosX = prevState['box1PosXinit'] + tiltRound*10
      newState['box1Pos'] = box1PosX + " 0.5 -3"
      let box2PosX = prevState['box2PosXinit'] + tiltRound*10
      newState['box2Pos'] = box2PosX + " 0.5 -3"
      let box3PosX = prevState['box3PosXinit'] + tiltRound*10
      newState['box3Pos'] = box3PosX + " 0.5 24"
      let planePosX = prevState['planePosXinit'] + tiltRound*10
      newState['planePos'] = planePosX + " 0 -4"
      return newState
    })
    axios.get(get_url)
      .then(res => {
        if (res.data.eyeTilt) {
          this.setState({tilt: res.data.eyeTilt[0]})
        }
        else {
          console.log("No eye data received")
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  componentDidMount () {
    const user_db_id = this.props.match.params.user_db_id
    let get_url = 'http://localhost:8000/api/getdata/'+user_db_id+'/'
    axios.get(get_url)
      .then(res => {
        this.setState({loading: false})
      })
      .catch(err => {
        console.log(err)
        this.setState({loading: true})
      })
    setInterval(this.get_user_pos, 500)
    setInterval(this.gamePlay, 2000)
  }
  gamePlay = () => {
    let newposx = this.randomposx()
    let pos = newposx + " 1.25 -5"
    this.setState(prevState => {
      const newState = prevState;
      let new_dict = {'pos': pos, 't': prevState['t']}
      newState['sphere_list'].push(new_dict)
      return newState
    })
  }
  randomposx = () => {
    return Math.random() * 20 - 10
  }
  render () {
    const {tilt, sphere, plane, loading, sphere_list, t, box1Pos, box2Pos, box3Pos, planePos} = this.state

    if (loading)
      return (<div>Game loading...</div>)
    else
      return (
        <Scene>
          <Box position={box1Pos} rotation="0 0 0" color="#4CC3D9" depth="48" height="2" width="1" shadow></Box>
          <Box position={box2Pos} rotation="0 0 0" color="#4CC3D9" depth="48" height="2" width="1" shadow></Box>
          <Box position={box3Pos} rotation="0 0 0" color="#4CC3D9" depth="1" height="2" width="24" shadow></Box>
          {sphere_list.map((val, i) => {
            let posAr = val.pos.split(" ")
            let posZ = Number(posAr[2]) + t - val.t
            let newPos = posAr[0] + " " + posAr[1] + " " + posZ
            return (<Icosahedron position={newPos} radius={sphere.rad} color={sphere.color} shadow></Icosahedron>)
          })}
          <Plane position={planePos} rotation={plane.rot} width={plane.width} height={plane.ht} color={plane.color} shadow></Plane>
          <Sky color="#ECECEC"></Sky>
          <Text value="Welcome to the game in VR" align="center" position="0 2.3 -1.5" color="#7BC8A4"></Text>
        </Scene>
        )
  }
}

export default VrBodyComponent;
